# Portfolio

This repo houses snippets and sketches of various designs and projects I have been involved in. Most 2D graphics have been created using SVG software called Inkscape. You will also find screenshots of cross-platform 2D/3D applications and games I have programmed in pure C99/C++. 

# Nomenclature

- MRF - Material Recycling Facility

- EfW - Energy from Waste

- P&ID/PID - Piping & Instrumentation Diagram

- GUI - Graphical User Interface

# - Sketches & Renders

### EfW furance door render (isometric)
![Furnace door iso](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/furnace_door_render.png)

### EfW furance door render (frontal)
![Furnace door front](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/Front_View_Labelled.png)

### MRF simplified sketch
![MRF](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/mrf_mehdi.png)

### EfW steam cycle simplified P&ID
![Steam cycle](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/efw_steam_lines_sketch.png)

### Generic EfW furance first pass P&ID
![1st pass](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/furnace_mehdi.png)

### EfW flue gas filtration (bag house filters) P&ID
![BHF](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/efw_filtering_mehdi.png)

### Various engineering component illustrations
![Engineering comps](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/rect6866.png)

# - 2D/3D Applications & Games

### Custom C++ 3D renderer with GUI (OpenGL)
![c++ renderer 1](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/c++_renderer_mehdi.png)

### Raycasted vehicle playground demo (C++/OpenGL) 
![c++ renderer 1](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/wagon_bounce.gif)
![c++ renderer 1](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/sedan_bounce.gif)

### OpenGL painting app powered by GFX card
![GL painting app](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/GL-painting.gif)

### OpenCL multi-pass image processing with GUI frontend (graphics card powered)
![opencl GUI](https://gitlab.com/mhd1msyb/portfolio/-/raw/main/OpenCL_GUI_frontend.gif)


